import argparse
import configparser
from kbai.incident_data_prep import *
import kbai.s3_utils

def main(in_data_key, out_data_key):
    config = configparser.ConfigParser()
    config.read('./config.ini')
    bucket = config['S3'].get('bucket', 'name_of_s3_bucket')
    raw_df = kbai.s3_utils.read_csv(bucket, in_data_key)
    out_df = (raw_df.pipe(rename_columns)
                    .pipe(keep_english_only)
                    .pipe(drop_duplicate_rows)
                    .pipe(drop_na_columns)
                    .pipe(merge_rows_with_same_index)
                    .pipe(split_incident_created_date)
                    .pipe(combine_description_resolution_event_details)
                    .pipe(to_lowercase)
                    .pipe(get_kb_ids))

    kbai.s3_utils.write_csv(out_df, bucket, out_data_key)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Takes in incident data extracted from Xtreme and \
        runs a series of consolidation and transformation steps on it. Both the raw \
        incident data and the output are stored under the same bucket defined in the \
        config.ini file')
    parser.add_argument("raw_incident_data_filename", type=str, help="Name of the raw incident data file as stored in S3")
    parser.add_argument("processed_incident_data_filename", type=str, help="Name of the processed incident data file as it will be stored in S3")
    args = parser.parse_args()
    main(args.raw_incident_data_filename, args.processed_incident_data_filename)