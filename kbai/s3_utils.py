import re
import boto3
import numpy as np
import pandas as pd
from sagemaker import get_execution_role

def read_csv(bucket, data_key):
    data_location = 's3://{}/{}'.format(bucket, data_key)
    return pd.read_csv(data_location)

def write_csv(dataframe, bucket, data_key):
    data_location = 's3://{}/{}'.format(bucket, data_key)
    dataframe.to_csv(data_location)