import re
import boto3
import nltk
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
from sagemaker import get_execution_role
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet


# functions for preprocessing KB data
def rename_columns(in_df):
    names_mapping = {
        'KB Number': 'kb_id',
        'Title': 'kb_title',
        'Published Date': 'kb_published_date',
        'Type': 'kb_type',
        'Affected Product': 'kb_affected_product',
        'Affected Release': 'kb_affected_release',
        'CLEAN Description': 'kb_description',
        'CLEAN Steps To Reproduce': 'kb_steps_to_reproduce',
        'CLEAN Workaround': 'kb_workaround',
        'CLEAN Resolution': 'kb_resolution',
        'CLEAN Note': 'kb_notes',
        'CLEAN Legacy Detailed Description': 'legacy_description',
        'CLEAN Legacy Resolution': 'legacy_resolution',
        'CLEAN Legacy Published Contents': 'legacy_published_content',
        'CLEAN Legacy Contents': 'legacy_content'}
    return in_df.rename(columns=names_mapping)


def drop_na_columns(in_df):
    return in_df.dropna(axis='columns', how='all').fillna('')


def drop_duplicate_rows(in_df):
    return in_df.drop_duplicates()


def remove_stop_words(text):
    stop_words = set(stopwords.words('english'))
    stop_words.update(['the', 'be', 'to', 'a', 'of', 'at', 'hi', 'hello',
                       'greetings', 'sunsystems', 'please', 'asap', 'thank',
                       'you', 'thanks', 'infor', 'report', 'error', 'run',
                       'message', 'fail', 'set', 'available', 'sun', 'bring',
                       'give', 'already', 'use', 'unable', 'able', 'get',
                       'could', 'use', 'could', 'found', 'via', 'may', 'try'
                       'change', 'option', 'number', 'data', 'value', 'within',
                       'dat', 'data', 'is', 'the', 'to'])
    word_tokens = word_tokenize(text)
    filtered_sentence = [w for w in word_tokens if(w not in stop_words)]
    return " ".join(set(filtered_sentence))


def apply_remove_stop_words_to_dataframe(in_df):
    cols = ['kb_title', 'kb_description',
            'kb_resolution', 'kb_steps_to_reproduce']
    for col in cols:
        in_df[col] = in_df[col].apply(lambda x: remove_stop_words(x))
    return in_df


def to_lowercase(in_df):
    cols = ['kb_title', 'kb_description',
            'kb_resolution', 'kb_steps_to_reproduce']
    for col in cols:
        in_df[col] = in_df[col].str.lower()
    return in_df


# some columns contain HTML text format: extracting the text
def html_to_text(html):
    """extract text out of html format
    
    Arguments:
        html {string} -- [text in html format]
    
    Returns:
        [string] -- [extracted text]
    """
    soup = BeautifulSoup(html, "lxml")
    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.extract()    # rip it out
    # get text
    text = soup.get_text()
    return text


def apply_extract_clean_text(in_df):
    cols = ['kb_title', 'kb_description',
            'kb_resolution', 'kb_steps_to_reproduce']
    for col in cols:
        in_df[col] = in_df[col].astype('str').apply(\
            lambda x: html_to_text(x)).str.replace('\W', ' ').str.replace(\
                            '\d+', ' ').apply(lambda x: re.sub(' +', ' ', x))
    return in_df


# map NLTK’s POS tags to the format wordnet lemmatizer would accept
def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag, wordnet.NOUN)


def lemmatize_dataframe_text(in_df):
    cols = ['kb_title', 'kb_description',
            'kb_resolution', 'kb_steps_to_reproduce']
    for col in cols:
        w_lemmatizer = WordNetLemmatizer()
        in_df[col] = in_df[col].apply(
            lambda x: ' '.join([w_lemmatizer.lemmatize(w, get_wordnet_pos(w))
                                for w in nltk.word_tokenize(x)]))
    return in_df


def combine_kb_content(in_df):
    in_df['kb_content'] = in_df['kb_title'] + ' ' + in_df['kb_description'] +\
                          ' ' + in_df['kb_resolution'] + ' ' +\
                          in_df['kb_steps_to_reproduce']
    cols2drop = ['kb_description', 'kb_resolution', 'kb_steps_to_reproduce']
    return in_df.drop(columns=cols2drop)


def clean_up_dataframe(in_df):
    in_df.drop(columns=['kb_title', 'kb_workaround', 'kb_notes',
                        'legacy_description', 'legacy_resolution', 'legacy_published_content',
                        'legacy_content'])
    # raw_df is a global dataset, in which the raw data was loaded
    in_df['kb_title'] = raw_df['Title'].str.lower()
    return in_df