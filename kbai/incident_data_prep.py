import re
import boto3
import numpy as np
import pandas as pd
from sagemaker import get_execution_role
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

def rename_columns(in_df):
    names_mapping = {
        'Incident_ID': 'incident_id',
        'Incident_Created_Date': 'incident_created_date',
        'Customer_Name': 'customer_name',
        'Country': 'country_name',
        'Customer_Type': 'customer_type',
        'Product_Line': 'product_line',
        'Product': 'product_name', 
        'Summary': 'incident_summary',
        'Description': 'incident_description',
        'Severity': 'incident_severity',
        'Resolution_Type': 'resolution_type',
        'Resolution': 'incident_resolution', 
        'Event_Details': 'event_details' }
    return in_df.rename(columns = names_mapping)

def drop_na_columns(in_df):
    return in_df.dropna(axis = 'columns', how = 'all')

def drop_duplicate_rows(in_df):
    return in_df.drop_duplicates()

def keep_english_only(in_df):
    english_speaking = ['Saudi Arabia', 'Ireland', 'United Kingdom', 'Egypt', 'Switzerland', 'Philippines', 'Thailand',
        'New Zealand', 'Hong Kong', 'Qatar', 'Oman', 'Netherlands', 'Uganda', 'Italy', 'Poland', 'Bahamas', 'Russian Federation',
        'Singapore', 'Ghana', 'Canada', 'Kenya', 'Malaysia', 'Myanmar', 'Botswana', 'Serbia', 'Macau', 'Kenya',
        'Australia', 'Nigeria', 'Lebanon', 'India', 'Barbados', 'Luxembourg', 'Greece', 'Bermuda', 'Hungary', 'Côte d\'Ivoire',
        'USA', 'United Arab Emirates', 'Bahrain', 'Kuwait', 'Singapore', 'Nigeria', 'Brunei', 'Cayman Islands']
    return in_df[(in_df['country_name'].isin(english_speaking))]

def merge_rows_with_same_index(in_df):
    grouped_df = in_df.groupby(['incident_id'])
    return grouped_df.aggregate({ 'incident_id': lambda x: x.count(),
                      'incident_created_date': lambda x: pd.to_datetime(x.unique()),
                      'customer_name': lambda x: x.unique(),
                      'country_name': lambda x: x.unique(),
                      'product_name': lambda x: x.unique(),
                      'incident_summary': lambda x: x.unique(),
                      'incident_severity': lambda x: x.unique(),
                      'incident_description': lambda x: x.unique(),
                      'incident_resolution': lambda x: x.unique(),
                      'resolution_type': lambda x: x.unique(),
                      'event_details': lambda x: x.str.cat(sep=' --- ') }).rename(
        columns = {'incident_id': 'distinct_entries_count'})

def split_incident_created_date(in_df):
    in_df[['year_create', 'month_create']] = in_df['incident_created_date'].apply(
        lambda x: pd.Series(x.strftime("%Y,%m").split(",")))
    return in_df.drop(columns = ['incident_created_date'])

def combine_description_resolution_event_details(in_df):
    in_df['description_resolution_event_details'] = in_df['incident_description'] + ' --- ' + in_df['event_details'] + ' --- ' + in_df['incident_resolution']
    in_df['description_resolution_event_details'] = in_df['description_resolution_event_details'].fillna('')
    return in_df.drop(columns = ['event_details'])

def remove_stop_words(text):
    stop_words = set(stopwords.words('english'))
    stop_words.update(['hi', 'hello', 'greetings', 'sunsystems', 
                       'please', 'asap', 'thank', 'you', 'thanks'])
    word_tokens = word_tokenize(text)
    filtered_sentence = [w for w in word_tokens if(w.isalpha() and w not in stop_words)]
    return " ".join(filtered_sentence)

def apply_remove_stop_words_to_dataframe(in_df):
    in_df['description_resolution_event_details'] = in_df['description_resolution_event_details'].apply(remove_stop_words)
    in_df['description_resolution_event_details'] = in_df['description_resolution_event_details'].str.replace('happy new year', '')
    return in_df

def to_lowercase(in_df):
    in_df['description_resolution_event_details'] = in_df['description_resolution_event_details'].str.lower()
    return in_df

def list_of_prefixed_ids_to_ids(list_kb_ids):
    KB = 'KB'
    temp = [x.strip(KB).strip() for x in list_kb_ids]
    if(len(temp) > 0 ):
        result = '|'.join(temp)
    else:
        result = '0'
    return result

def get_kb_ids(in_df):
    KB = 'KB'
    in_df['kb_raw'] = (in_df['incident_resolution']
                       .apply(lambda x: re.findall(KB + ' ' + r'\d+' + ' ', str(x)))
                       .apply(list_of_prefixed_ids_to_ids))
    in_df = in_df.reset_index()
    return in_df