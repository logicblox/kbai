from setuptools import find_packages, setup

setup(
    name='kbai',
    version='1.0.0',
    description='Knowledge Base Articles Inference',
    author='CloudOPS Team - Infor, Inc.',
    packages=find_packages(include=['kbai']),
    scripts=['orchestrator.py'],
    include_package_data=True,
    zip_safe=False
)